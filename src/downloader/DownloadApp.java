package downloader;

import javax.swing.SwingUtilities;

/**
 * Launch the URL downloader application.
 */
public class DownloadApp {

	/**
	 * Main method to start the user interface.
	 */

	public static void main(String[] args) {
		DownloaderUI ui = new DownloaderUI();
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {

				ui.run();
			}
		});
	}
}
